﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VehicleRegistration.Interfaces;

namespace VehicleRegistration.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext context;

        public UnitOfWork(ApplicationDbContext context)
        {
            this.context = context;
        }

        public IVehicleRepository VehicleRepository => new SqlVehicleRepository(context);

        public async Task<bool> Save()
        {

            return await context.SaveChangesAsync() > 0;
        }
    }
}
