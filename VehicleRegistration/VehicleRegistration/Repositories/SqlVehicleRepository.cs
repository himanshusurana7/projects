﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VehicleRegistration.Entities;
using VehicleRegistration.Interfaces;

namespace VehicleRegistration.Repositories
{
    public class SqlVehicleRepository : IVehicleRepository
    {
        private readonly ApplicationDbContext context;

        public SqlVehicleRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public async Task<Vehicle> add(Vehicle vehicle)
        {
            await context.Vehicles.AddAsync(vehicle);
            return vehicle;
        }

        public async Task<Vehicle> delete(string rc)
        {
            var vehicle = await context.Vehicles.FirstOrDefaultAsync(x => x.RcNumber == rc);
            if (vehicle == null)
            {
                return null;
            }
            else
            {
                context.Vehicles.Remove(vehicle);
                return vehicle;
            }
        }

        public async Task<List<Vehicle>> Get()
        {
            var vehicle = await context.Vehicles.ToListAsync();
            return vehicle;
        }

        public async Task<Vehicle> GetById(string rc)
        {
            var vehicle = await context.Vehicles.FirstOrDefaultAsync(x => x.RcNumber == rc);
            return vehicle;
        }

        public async Task<Vehicle> update(Vehicle vehicle)
        {
            var exists = await context.Vehicles.AnyAsync(x => x.RcNumber == vehicle.RcNumber);
            if(!exists)
            {
                return null;
            }
            else
            {
                context.Entry(vehicle).State = EntityState.Modified;
                return vehicle;
            }
        }
    }
}
