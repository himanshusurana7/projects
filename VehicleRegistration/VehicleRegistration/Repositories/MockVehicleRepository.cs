﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VehicleRegistration.DTOs;
using VehicleRegistration.Entities;
using VehicleRegistration.Interfaces;

namespace VehicleRegistration.Repositories

{
    public class MockVehicleRepository : IVehicleRepository
    {
        private List<Vehicle> vehicleList;

        public MockVehicleRepository()
        {
            vehicleList = new List<Vehicle>()
            {
                new Vehicle(){RcNumber = "WB20AP1234", Owner = "Mukesh Surana",RTO = "Alipore WB20",
                    Model = "Pulsar 150",ChasisNumber= "XyaHasadasDEfGHijk01501" },
                new Vehicle(){RcNumber = "WB20AT2214", Owner = "Sushil Surana",RTO = "Alipore WB20",
                    Model = "Maestro Edge",ChasisNumber= "XyaHasadasDEf1232ijk01501" }
            };
        }

        public async Task<List<Vehicle>> Get()
        {
           return vehicleList;
        }
        public async Task<Vehicle> GetById(string rc)
        {
            return vehicleList.FirstOrDefault(x => x.RcNumber == rc);
        }
        public async Task<Vehicle> add(Vehicle vehicle)
        {
            vehicleList.Add(vehicle);
            return vehicle;
        }
        public async  Task<Vehicle> update(Vehicle vehicle)
        {
            var v = vehicleList.FirstOrDefault(x => x.RcNumber == vehicle.RcNumber);
            if(v != null)
            {
                v.Owner = vehicle.Owner;
                v.RTO = vehicle.RTO;
                v.Model = vehicle.Model;
                v.ChasisNumber = vehicle.ChasisNumber;
            }
            return v;
        }

        public async Task<Vehicle> delete(string rc)
        {
            var v = vehicleList.FirstOrDefault(x => x.RcNumber == rc);
            if (v != null)
            {
                vehicleList.Remove(v);
            }
            return v;
        }
    }
}
