﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleRegistration.DTOs
{
    public class VehicleDTO
    {
        public string RcNumber { get; set; }
        public string Owner { get; set; }
        public string Model { get; set; }
        public string RTO { get; set; }
        public string ChasisNumber { get; set; }
    }
}
