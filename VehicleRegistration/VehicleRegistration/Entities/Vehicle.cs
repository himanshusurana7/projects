﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleRegistration.Entities
{
    public class Vehicle
    {
        [Key]
        public string RcNumber{ get; set; }
        public string Owner{ get; set; }
        public string Model { get; set; }
        public string RTO { get; set; }
        public string ChasisNumber { get; set; }
    }
}
