﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VehicleRegistration.DTOs;
using VehicleRegistration.Entities;

namespace VehicleRegistration.Helper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Vehicle, VehicleDTO>().ReverseMap();
        }
    }
}
