using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;

namespace VehicleRegistration.Helper
{
    public class Log : ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            File.AppendAllText("logs.txt","Action Executed "+context.ActionDescriptor.DisplayName +"\n");
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            File.AppendAllText("logs.txt","Action Executing "+context.ActionDescriptor.DisplayName+"\n");
        }


        public override void OnResultExecuted(ResultExecutedContext context)
        {
            File.AppendAllText("logs.txt","Results Executed "+context.ActionDescriptor.DisplayName+"\n");
        }

        public override void OnResultExecuting(ResultExecutingContext context)
        {
            File.AppendAllText("logs.txt","Results Executing "+context.ActionDescriptor.DisplayName+"\n");
        }
    }
}