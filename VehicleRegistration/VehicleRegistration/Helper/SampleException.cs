using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace VehicleRegistration.Helper
{
    public class SampleException : ExceptionFilterAttribute , IExceptionFilter
    {
        public override void OnException(ExceptionContext context)
        {
            File.AppendAllText("logs.txt",context.ExceptionDispatchInfo.SourceException.StackTrace);
        }

    }
}