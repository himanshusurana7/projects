﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VehicleRegistration.DTOs;
using VehicleRegistration.Entities;
using VehicleRegistration.Helper;
using VehicleRegistration.Interfaces;

namespace VehicleRegistration.Controllers
{
    [Log]
    [Route("/api/vehicles")]
    [ApiController]
    public class VehicleController : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly IUnitOfWork unitOfWork;

        public VehicleController(IMapper mapper,IUnitOfWork unitOfWork)
        {
            this.mapper = mapper;
            this.unitOfWork = unitOfWork;
        }

        [HttpGet]//get request
        public async Task<ActionResult<List<VehicleDTO>>> Get()
        {
            var vehicle = await unitOfWork.VehicleRepository.Get();
            var vehicleDTO = mapper.Map<List<VehicleDTO>>(vehicle);

            return vehicleDTO;
        }

        [SampleException]
        [HttpGet("{rc}", Name ="GetById")]
        public async Task<ActionResult<VehicleDTO>> GetById([FromRoute] string rc)
        {
            var vehicle = await unitOfWork.VehicleRepository.GetById(rc);
            if(vehicle==null)
            {     
                throw new Exception("exception filter trial");
                //return NotFound("No such RC exists");
            }
            var vehicleDTO = mapper.Map<VehicleDTO>(vehicle);
            return vehicleDTO;
        }

        [HttpPost("add")]
        public async Task<ActionResult> Post([FromBody] VehicleDTO vehicleDTO)
        {
            var vehicle = mapper.Map<Vehicle>(vehicleDTO);
            vehicle = await unitOfWork.VehicleRepository.add(vehicle);
            await unitOfWork.Save();
            return Created("/api/vehicles/{vehicle.RcNumber}",vehicle);
        }

        [HttpPut("update/{rc}")]
        public async Task<ActionResult> Put(string rc,[FromBody] VehicleDTO vehicleDTO)
        {
            var vehicle = mapper.Map<Vehicle>(vehicleDTO);
            vehicle = await unitOfWork.VehicleRepository.update(vehicle);
            if(vehicle == null)
            {
                return NotFound("No such RC exists");
            }
            await unitOfWork.Save();
            return Created($"/api/vehicles/{vehicle.RcNumber}", vehicle);
        }

        [HttpDelete("remove/{rc}")]
        public async Task<ActionResult> Delete(string rc)
        {
            var vehicle = await unitOfWork.VehicleRepository.delete(rc);
            if(vehicle == null)
            {
                return NotFound("No such RC exists");
            }
            else
            {
                await unitOfWork.Save();
                return Ok("Entry Deleted sucessfully.");
            }
        }
    }
}
