﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VehicleRegistration.Entities;

namespace VehicleRegistration.Interfaces
{
    public interface IVehicleRepository
    {
        Task<Vehicle> add(Vehicle vehicle);
        Task<Vehicle> delete(string rc);
        Task<List<Vehicle>> Get();
        Task<Vehicle> GetById(string rc);
        Task<Vehicle> update(Vehicle vehicle);
    }
}
