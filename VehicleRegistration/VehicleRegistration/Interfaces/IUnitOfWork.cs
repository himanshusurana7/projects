﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleRegistration.Interfaces
{

    public interface IUnitOfWork
    {
        IVehicleRepository VehicleRepository { get; }

        Task<bool> Save();
    }
}
