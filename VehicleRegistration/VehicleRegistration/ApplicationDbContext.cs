﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using VehicleRegistration.Entities;

namespace VehicleRegistration
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {
        }
        //this will create a table named Vehicles will all the columns as mentioned in Vehicle.cs
        public DbSet<Vehicle> Vehicles { get; set; }
    
    }
}
