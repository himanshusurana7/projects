﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecordPractice
{
    public delegate int AddDelegate(int a, int b);
    class DelegatePractice
    {
        public void Add(int a, int b,AddDelegate obj)
        {
            Console.WriteLine(obj(a, b));
        }
    }
}
