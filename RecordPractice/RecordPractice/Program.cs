﻿using System;

namespace RecordPractice
{
    class Program
    {
        public record Person
        {
            public string Fname { get; init; }
            public string Lname { get; init; }
            public string Phno { get; set; }

            public void DisplayInfo()
            {
                Console.WriteLine("First name: {0} \nLast name: {1} \nPhone Number: {2} ",this.Fname,this.Lname,this.Phno);
            }
        }
        static void Main(string[] args)
        {
            //object[] list = { 1, 2, "abc" };
            Person Nikhil = new()
            {
                Fname= "Nikhil",
                Lname = "Vardhan",
                Phno = "1122334400"
            };
            Nikhil.Phno = "9988773322";
            Nikhil.DisplayInfo();
            Console.WriteLine("=====================");
            AddDelegate obj1 = new AddDelegate(Process1);
            AddDelegate obj2 = new AddDelegate(Process2);
            AddDelegate Chain , MinusChain;
            Chain = obj1+obj2;
            MinusChain = Chain - obj1;
            int Process1(int a,int b)
            {
                Console.WriteLine("Process 1 output = {0}",a-b);
                return a - b;
            }
            int Process2(int a, int b)
            {
                Console.WriteLine("Process 2 output = {0}",a+b);
                return a+b;
            }
            DelegatePractice d = new ();
            d.Add(3,2,obj1);
            d.Add(3,2,obj2);
            d.Add(7,2,Chain);
            d.Add(3,2,MinusChain);
            //Console.WriteLine(list[2]);
        }
    }
}
