using System;

namespace GenericsPractice
{
    /// <summary>
    /// this class will help you create a generic array.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    class GenericArray<T>
    {
        private int count;
        public T[] obj = new T[7];
            
        public GenericArray()
        {
            count = 0;
        }
        /// <summary>
        /// this function will help us add elements to an array.
        /// </summary>
        /// <param name="ele"></param>
        public void Add(T ele)
        {
            if(count<7)
            {
                this.obj[count] = ele;
                count++;
            }
            else
            {
                Console.WriteLine("Memory Limit exceeded");
            }
        }

        public void Print()
        {
            for(int i=0;i<count;i++)
            {
                Console.WriteLine(obj[i]);
            }
        }
    }
}