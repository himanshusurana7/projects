﻿using System;

namespace GenericsPractice
{
    class Program
    {
        static void Main(string[] args)
        {
            GenericArray<int> IntArray = new GenericArray<int>();
            for(int i=7;i>0;i--)
            {
                IntArray.Add(i);
            
            }
            IntArray.obj.MySort();
            IntArray.Print();
            GenericArray<string> StringArray = new GenericArray<string>();
            StringArray.Add("Himanshu");
            StringArray.Add("Surana");
            StringArray.Print();
        }
    }
}
