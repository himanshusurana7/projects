using System;

namespace GenericsPractice
{
    /// <summary>
    /// Over here i am implementing Open CLosed principle through extension method
    /// </summary>
    public static class ArrayHelper
    {
        private static int i,j,temp;
        /// <summary>
        /// this function will sort an integer array
        /// </summary>
        /// <param name="arr"></param>
        public static void MySort(this int[] arr)
        {
            
            for(i=0;i<6;i++)
            {
                for(j=0;j<6;j++)
                {
                    if(arr[j+1]<arr[j])
                    {   
                        temp = arr[j];
                        arr[j] = arr[j+1];
                        arr[j+1] = temp;
                    }
                }
            }
        }
    }
}