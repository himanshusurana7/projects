﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace LINQPractice
{
    class Program
    {
        ///<summary>
        ///unsafe code      
        ///</summary>
        /*public static unsafe void Print()
        {
             int a = 100;
             int* b = &a;
             Console.WriteLine("{0} {1}",a,(int)b);
        }*/
        static void Main(string[] args)
        {
            List<Vehicles> DataSource = new List<Vehicles>();
            DataSource.Add(new Vehicles("WB20BH8074","Surana Textiles","Hero Xtreme 160R",1));
            DataSource.Add(new Vehicles("WB20AT2214","Sushil Surana","Hero Meastro Edge",5));
            DataSource.Add(new Vehicles("WB02AK0567","Mukesh Surana","I20 Sportz",4));
            DataSource.Add(new Vehicles("WB20AT2087","Suraj Surana","Activa 125",5));
            //Console.WriteLine("{0} {1} {2} {3}",DataSource[0].RcNo,DataSource[1].RcNo,DataSource[2].RcNo,DataSource[3].RcNo);

            var QuerySyntax = from vehicle in DataSource where vehicle.Age >=3 select vehicle.RcNo;
            foreach (var item in QuerySyntax)
            {
                Console.WriteLine(item); 
            }
            Console.WriteLine("==============");

            //var MethodSyntax = DataSource.SelectAll(vehicle=> vehicle.RcNo).Where(vehicle => vehicle.Owner="Himanshu");
            var MethodSyntax = DataSource.Where(rd=>rd.Owner!="Humanshu surana").Select(vehicle => vehicle.RcNo);
            foreach( var item in MethodSyntax)
            {
                Console.WriteLine(item);
            } 
 
            Console.WriteLine("=============="); 

            var MixedSyntax = (from vehicle in DataSource select vehicle).Where(vehicle=> vehicle.Age < 5).Select(vehicle=> new {vehicle.RcNo,vehicle.Owner});
            foreach(var item in MixedSyntax)
            {
                Console.WriteLine("RcNo = {0} Name = {1}",item.RcNo,item.Owner);
            }  
            //Print(); 
            DataSource.Sort();
            foreach( var item in DataSource)
            {
                Console.WriteLine("{0} {1}",item.RcNo,item.Age);
            }

        }
 
    }
}
