﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQPractice
{
    class Vehicles:IComparable<Vehicles>
    {
        public string RcNo {get;set;}
        public string Owner {get;set;}
        public string Model {get;set;}
        public int Age {get;set;}

        public Vehicles(string RcNo, string Owner, string Model,int Age)
        {
            //[MaxLength(10),MinLength(10)]
            this.RcNo = RcNo;
            this.Owner = Owner;
            this.Model = Model;
            this.Age = Age;
        }

        public int CompareTo(Vehicles other)
        {
            if(this.Age>other.Age)
            {
                return 1;
            }
            else if(this.Age == other.Age)
            {
                return 0;
            }
            else{
                return -1;
            }
        }
    }
}
