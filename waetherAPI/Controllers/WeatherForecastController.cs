﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using waetherAPI.Entities;
using waetherAPI.Helpers;
using waetherAPI.Interfaces;

namespace waetherAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly ILogger<WeatherForecastController> _logger;
        private readonly IHttpClientFactory httpClientFactory;
        private readonly ITokenRefresher tokenRefresher;
        public IJwtAuthenticationManager JwtAuthenticationManager;

        public WeatherForecastController(IJwtAuthenticationManager jwtAuthenticationManager,ILogger<WeatherForecastController> logger , IHttpClientFactory httpClientFactory,ITokenRefresher tokenRefresher)
        {
            JwtAuthenticationManager = jwtAuthenticationManager;
            _logger = logger;
            this.httpClientFactory = httpClientFactory;
            this.tokenRefresher = tokenRefresher;
        }

        [Authorize]
        [HttpGet]
        public async Task<String> Get([FromQuery]string city)
        {

            var url = $"?key=f3529f84aa624b7daf163436210412&q={city}&aqi=no";
            var httpClient = httpClientFactory.CreateClient("weather");
            var response = await httpClient.GetAsync(url);
            return await response.Content.ReadAsStringAsync();  
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody] UserCred credentials)
        {
            AuthenticationResponse tokens = JwtAuthenticationManager.Authenticate(credentials.username,credentials.password);
            if(tokens == null)
            {
                return Unauthorized();
            }
            return Ok(tokens);
        }

        [AllowAnonymous]
        [HttpPost("refresh")]
        public IActionResult Refresh([FromBody] AuthenticationResponse tokens)
        {
            AuthenticationResponse newTokens = tokenRefresher.Refresh(tokens);
            if(newTokens == null)
            {
                return Unauthorized();
            }
            return Ok(newTokens);
        }
    }
}
