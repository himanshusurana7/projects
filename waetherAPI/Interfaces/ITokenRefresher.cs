using waetherAPI.Entities;

namespace waetherAPI.Helpers
{
    public interface ITokenRefresher
    {
        AuthenticationResponse Refresh(AuthenticationResponse tokens);
    }
}