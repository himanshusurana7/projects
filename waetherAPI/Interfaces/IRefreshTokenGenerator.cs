namespace waetherAPI.Helpers
{
    public interface IRefreshTokenGenerator
    {
        string GenerateToken();
    }
}