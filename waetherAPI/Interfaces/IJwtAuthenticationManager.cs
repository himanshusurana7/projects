using System.Collections.Generic;
using System.Security.Claims;
using waetherAPI.Entities;

namespace waetherAPI.Interfaces
{
    public interface IJwtAuthenticationManager
    {
        AuthenticationResponse Authenticate(string username , string password);
        AuthenticationResponse Authenticate(string username , Claim[] claims);
        IDictionary<string , string> RefreshTokensOfUsers{get;set;}
    }
}