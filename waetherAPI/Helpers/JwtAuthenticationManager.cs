using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using waetherAPI.Entities;
using waetherAPI.Interfaces;

namespace waetherAPI.Helpers
{
    public class JwtAuthenticationManager : IJwtAuthenticationManager
    {
        private readonly string key;
        private readonly IRefreshTokenGenerator refreshTokenGenerator;
        private IDictionary<string , string> users = new Dictionary<string , string>
        {
            {"test1" , "password1"},
            {"test2" , "password2"}   
        };
        public IDictionary<string , string> RefreshTokensOfUsers {get;set;}

        public JwtAuthenticationManager(string key , IRefreshTokenGenerator refreshTokenGenerator)
        {
            this.key = key;
            this.refreshTokenGenerator = refreshTokenGenerator;
            RefreshTokensOfUsers = new Dictionary<string,string>();
        }

        public AuthenticationResponse Authenticate(string username, string password)
        {
            if(users.Any(x=>x.Key == username && x.Value == password))
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var tokenKey = Encoding.ASCII.GetBytes(key);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(
                        new Claim[] {
                            new Claim(ClaimTypes.Name,username)
                            }
                    ),
                    Expires = DateTime.UtcNow.AddHours(1),
                    SigningCredentials = new SigningCredentials(
                        new SymmetricSecurityKey(tokenKey),
                        SecurityAlgorithms.HmacSha256Signature
                    )
                }; 
                
                var refreshToken = refreshTokenGenerator.GenerateToken();
                var token = tokenHandler.CreateToken(tokenDescriptor);
                RefreshTokensOfUsers.Add(username,refreshToken);
                return new AuthenticationResponse{JwtToken = tokenHandler.WriteToken(token) ,  RefreshToken = refreshToken};
            }
            return null;
        }
        public AuthenticationResponse Authenticate(string username , Claim[] claims)
        {
            var jwtSecurityToken = new JwtSecurityToken(
                claims: claims,
                expires:DateTime.UtcNow.AddHours(1),
                signingCredentials : new SigningCredentials(
                    new SymmetricSecurityKey(Encoding.ASCII.GetBytes(key)),
                    SecurityAlgorithms.HmacSha256Signature
            ));
            var tokenHandler = new JwtSecurityTokenHandler();
            var refreshToken = refreshTokenGenerator.GenerateToken();
            if(RefreshTokensOfUsers.ContainsKey(username))
            {
                RefreshTokensOfUsers[username] = refreshToken;
            }
            return new AuthenticationResponse{JwtToken = tokenHandler.WriteToken(jwtSecurityToken) ,  RefreshToken = refreshToken};
            
            
        }
        
    }
}