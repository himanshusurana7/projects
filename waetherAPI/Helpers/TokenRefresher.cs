using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using waetherAPI.Entities;
using waetherAPI.Interfaces;

namespace waetherAPI.Helpers
{
    public class TokenRefresher:ITokenRefresher
    {
        private readonly string key;
        private readonly IJwtAuthenticationManager jwtAuthenticationManager;

        public TokenRefresher(string key , IJwtAuthenticationManager jwtAuthenticationManager)  
        {
            this.key = key;
            this.jwtAuthenticationManager = jwtAuthenticationManager;
        }

        public AuthenticationResponse Refresh(AuthenticationResponse tokens)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken validatedToken;
            var principal = tokenHandler.ValidateToken(tokens.JwtToken,
                new TokenValidationParameters{
                    ValidateAudience = false,
                    ValidateIssuer = false,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(this.key)),
                    ValidateIssuerSigningKey = true
                },out validatedToken
            );
            var newToken = validatedToken as JwtSecurityToken;
            if(newToken == null || !newToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256,System.StringComparison.InvariantCultureIgnoreCase))
            {
                throw new SecurityTokenException("Invalid Token");
            }
            var userName = principal.Identity.Name;
            if(tokens.RefreshToken != jwtAuthenticationManager.RefreshTokensOfUsers[userName])
            {
                throw new SecurityTokenException("Invalid Refresh Token");
            }
            return jwtAuthenticationManager.Authenticate(userName,principal.Claims.ToArray());
        }
    }
}