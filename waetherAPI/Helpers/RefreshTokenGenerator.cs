using System;
using System.Security.Cryptography;
using waetherAPI.Interfaces;

namespace waetherAPI.Helpers
{
    public class RefreshTokenGenerator : IRefreshTokenGenerator
    {
        public string GenerateToken()
        {
            var randomNumber = new byte[32];
            using(var randomNumberGenerated = RandomNumberGenerator.Create())
            {
                randomNumberGenerated.GetBytes(randomNumber);
                return Convert.ToBase64String(randomNumber);
            }
        }
    }
}