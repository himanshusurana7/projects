﻿using System;
using System.Linq;
using System.Reflection;

namespace ReflectionPractice
{
    class Program
    {
        static void Main(string[] args)
        {
            var assembly = Assembly.LoadFrom(@"C:\Users\himanshu.surana\ReflectionPractice\bin\Debug\net5.0\ReflectionPractice.dll"); 
            var Types = assembly.GetTypes();
            foreach (var type in Types)
            {
                Console.WriteLine(type); // accessing the various classes(types) present under this assembly.
            }
            
            Console.WriteLine("==========Fields==========");
            var Fields = Types[0].GetFields(BindingFlags.NonPublic | BindingFlags.DeclaredOnly | BindingFlags.Instance);
            foreach(var field in Fields)
            {
                Console.WriteLine(field);
            }
            //Creating the instance 
            var instance = Activator.CreateInstance(Types[0]);
            Fields[0].SetValue(instance,"Himanshu");

            Console.WriteLine("==========Methods==========");
            var Methods = Types[0].GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly);
            foreach(var method in Methods.Where(m=> !m.IsSpecialName))
            {
                Console.WriteLine(method);
                if(method.ReturnType.Name != "Void")
                {
                    var ReturnedValue = method.Invoke(instance,null);
                }
                else if(method.GetParameters().Length > 0)
                {
                    method.Invoke(instance, new[] {"Sunil"});
                }
                else
                {
                    method.Invoke(instance,null);
                }
            }
            
            Console.WriteLine("==========Properties==========");
            var Properties = Types[0].GetProperties();
            foreach(var property in Properties)
            {
                Console.WriteLine(property);
                var value = property.GetValue(instance);
                Console.WriteLine(value);
            }
            
        }
    }
}
