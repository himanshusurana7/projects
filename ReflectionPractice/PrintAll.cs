using System;
namespace ReflectionPractice
{
    class PrintAll
    {
        private string name;
        public void Print()
        {
            Console.WriteLine("Printing from public print");
        }

        public string GetName()
        {
            return this.name;
        }

        public void PrintName()
        {
            Console.WriteLine($"Name set as {this.name}");
        }

        public void Print(string name)
        {
            Console.WriteLine($"Name passed: {name}");
        }

        private void PrintPrivate()
        {
            Console.WriteLine("printing from private");
        }

        public string Name => name;

        public static string StaticName => "StaticName";

    }
}